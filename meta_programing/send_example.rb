class Account
  attr_accessor :name, :address, :email, :notes

  def assign_values values
    values.each_key do |k|
      self.send("#{k}=", values[k])
      # self.name = values[k]
    end
 end
end

user_info = {
  :name => "Pallavi",
  :address => "mandya",
  :email => "pallavi@gmail.com",
  :notes => "Welcome Note!"
}

account = Account.new
account.assign_values(user_info)
# account.name = user_info[:name]
puts account.inspect
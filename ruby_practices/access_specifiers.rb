class Access
	private
	def say_hi
		puts "hi"
	end

	 public
	 def say_hello
	 puts "hello"
	 puts "now calling private and protected methods here only"
	 say_hi
	 say_gm
	end

	 protected
	 def say_gm
	 	puts "Good Morning"
	 end
end



puts "Directly callig protected method"
Access.new.send(:say_gm)
puts "Directly callig private method"
Access.new.send(:say_hi)
puts "Directly callig public method"
Access.new.send(:say_hello)

a = Access.new
a. say_hello
# a.send(:say_hi)   we can call lk this also

	 #a. say_gm #error
	 #a. say_hi #error
begin
rescue
ensure
end
